# Urbs

Project to enable collective map-making from the ground up.

This project aims to use GPS enabled cellphones for acquiring geographical data.

It is about the inversion of thinking, instead of costly monolithic projects why not try open source distributed collaboration. I am aware that GPS need the satellites and that is not a substitute for GPS itself, but geographic information is now a need and not a luxury so people could get more involved in the processing and production of spatial data that is useful for inhabited spaces.

## The idea

* Use [USGS GMTED2010](https://www.usgs.gov/land-resources/eros/coastal-changes-and-impacts/gmted2010) as a base for elevation maps.
* Create vectorized contour lines.
* Use GPX data from popular running apps for enhancing map resolution.
* Provide statistical models for manage points as probability distributions.
* Provide source and infrastructure for handling and transforming data.